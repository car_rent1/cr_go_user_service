package grpc

import (
	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/service"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	user_service.RegisterMechanikServiceServer(grpcServer, service.NewMechanicService(cfg, log, strg, srvc))
	user_service.RegisterInvestorServiceServer(grpcServer, service.NewInvestorService(cfg, log, strg, srvc))
	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
