package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ClientService) Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.Client, err error) {

	i.log.Info("---CreateClient------>", logger.Any("req", req))

	pKey, err := i.strg.Client().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateClient->Client->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Client().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *ClientService) GetByID(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.Client, err error) {

	i.log.Info("---GetClientByID------>", logger.Any("req", req))

	resp, err = i.strg.Client().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClientByID->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) GetList(ctx context.Context, req *user_service.GetListClientRequest) (*user_service.GetListClientResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Client().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListClient->Client->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *ClientService) Update(ctx context.Context, req *user_service.UpdateClient) (resp *user_service.Client, err error) {
	i.log.Info("---UpdateClient------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Client().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateClient--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Client().GetByPKey(ctx, &user_service.ClientPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ClientService) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.ClientEmpty, err error) {

	i.log.Info("---DeleteClient------>", logger.Any("req", req))

	resp, err = i.strg.Client().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
