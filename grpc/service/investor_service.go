package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InvestorService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedInvestorServiceServer
}

func NewInvestorService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *InvestorService {
	return &InvestorService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *InvestorService) Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.Investor, err error) {

	i.log.Info("---CreateInvestor------>", logger.Any("req", req))

	pKey, err := i.strg.Investor().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateInvestor->Investor->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Investor().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *InvestorService) GetByID(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.Investor, err error) {

	i.log.Info("---GetInvestorByID------>", logger.Any("req", req))

	resp, err = i.strg.Investor().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetInvestorByID->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *InvestorService) GetList(ctx context.Context, req *user_service.GetListInvestorRequest) (*user_service.GetListInvestorResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Investor().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListInvestor->Investor->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *InvestorService) Update(ctx context.Context, req *user_service.UpdateInvestor) (resp *user_service.Investor, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Investor().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Investor().GetByPKey(ctx, &user_service.InvestorPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *InvestorService) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.InvestorEmpty, err error) {

	i.log.Info("---DeleteInvestor------>", logger.Any("req", req))

	resp, err = i.strg.Investor().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
