package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
)

type BranchService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedBranchServiceServer
}

func NewBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchService {
	return &BranchService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *BranchService) Create(ctx context.Context, req *user_service.CreateBranch) (resp *user_service.Branch, err error) {

	u.log.Info("-----------CreateBranch----------", logger.Any("req", req))

	id, err := u.strg.Branch().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateBranch ->Branch->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Branch().GetByID(ctx, &user_service.BranchPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdBranch->GetById", logger.Error(err))
		return
	}

	return
}

func (u *BranchService) GetByID(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Branch().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Branch->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *BranchService) GetList(ctx context.Context, req *user_service.GetListBranchRequest) (*user_service.GetListBranchResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Branch().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Branch->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *BranchService) Update(ctx context.Context, req *user_service.UpdateBranch) (resp *user_service.Branch, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Branch().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Branch->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Branch().GetByID(ctx, &user_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Branch->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *BranchService) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.BranchEmpty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Branch().Delete(ctx, &user_service.BranchPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Branch->Delete", logger.Error(err))
		return
	}

	return
}
