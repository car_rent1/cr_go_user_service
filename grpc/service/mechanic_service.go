package service

import (
	"context"

	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
)

type MechanicService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedMechanikServiceServer
}

func NewMechanicService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MechanicService {
	return &MechanicService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *MechanicService) Create(ctx context.Context, req *user_service.CreateMechanic) (resp *user_service.Mechanic, err error) {

	u.log.Info("-----------CreateMechanic----------", logger.Any("req", req))

	id, err := u.strg.Mechanic().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateMechanic ->Mechanic->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Mechanic().GetByID(ctx, &user_service.MechanicPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdMechanic->GetById", logger.Error(err))
		return
	}

	return
}

func (u *MechanicService) GetById(ctx context.Context, req *user_service.MechanicPrimaryKey) (resp *user_service.Mechanic, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Mechanic().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Mechanic->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *MechanicService) GetList(ctx context.Context, req *user_service.GetListMechanicRequest) (*user_service.GetListMechanicResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Mechanic().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Mechanic->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *MechanicService) Update(ctx context.Context, req *user_service.UpdateMechanic) (resp *user_service.Mechanic, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Mechanic().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Mechanic->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Mechanic().GetByID(ctx, &user_service.MechanicPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Mechanic->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *MechanicService) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) (resp *user_service.MechanicEmpty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Mechanic().Delete(ctx, &user_service.MechanicPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Mechanic->Delete", logger.Error(err))
		return
	}

	return
}
