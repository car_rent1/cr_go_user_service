package storage

import (
	"context"

	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	Mechanic() MechanicRepoI
	Branch() BranchRepoI
	Investor() InvestorRepoI
	Client() ClientRepoI
}

type MechanicRepoI interface {
	Create(context.Context, *user_service.CreateMechanic) (string, error)
	GetByID(context.Context, *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error)
	GetList(context.Context, *user_service.GetListMechanicRequest) (*user_service.GetListMechanicResponse, error)
	Update(context.Context, *user_service.UpdateMechanic) (int64, error)
	Delete(context.Context, *user_service.MechanicPrimaryKey) error
}

type BranchRepoI interface {
	Create(context.Context, *user_service.CreateBranch) (string, error)
	GetByID(context.Context, *user_service.BranchPrimaryKey) (*user_service.Branch, error)
	GetList(context.Context, *user_service.GetListBranchRequest) (*user_service.GetListBranchResponse, error)
	Update(context.Context, *user_service.UpdateBranch) (int64, error)
	Delete(context.Context, *user_service.BranchPrimaryKey) error
}
type InvestorRepoI interface {
	Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.InvestorPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.Investor, err error)
	GetAll(ctx context.Context, req *user_service.GetListInvestorRequest) (resp *user_service.GetListInvestorResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateInvestor) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) (*user_service.InvestorEmpty, error)
}

type ClientRepoI interface {
	Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.ClientPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.Client, err error)
	GetAll(ctx context.Context, req *user_service.GetListClientRequest) (resp *user_service.GetListClientResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateClient) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.ClientPrimaryKey) (*user_service.ClientEmpty, error)
}
