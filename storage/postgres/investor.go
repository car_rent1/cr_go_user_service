package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/helper"
)

type investorRepo struct {
	db *pgxpool.Pool
}

func NewInvestorRepo(db *pgxpool.Pool) *investorRepo {
	return &investorRepo{
		db: db,
	}
}

func (c *investorRepo) Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.InvestorPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "investor" (
				id,
				first_name,
				last_name,
				birthday,
				phone_number,
				balance,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.DateOfBirth,
		req.PhoneNumber,
		req.Balance,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.InvestorPrimaryKey{Id: id.String()}, nil

}

func (c *investorRepo) GetByPKey(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.Investor, err error) {
	query := `
		SELECT
			id,
			first_name,
			last_name,
			birthday,
			phone_number,
			balance,
			created_at,
			updated_at
		FROM "investor"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		firstName    sql.NullString
		lastName     sql.NullString
		birthday     sql.NullString
		phone_number sql.NullString
		balance      sql.NullFloat64
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&firstName,
		&lastName,
		&birthday,
		&phone_number,
		&balance,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Investor{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		DateOfBirth: birthday.String,
		PhoneNumber: phone_number.String,
		Balance:     float32(balance.Float64),
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *investorRepo) GetAll(ctx context.Context, req *user_service.GetListInvestorRequest) (resp *user_service.GetListInvestorResponse, err error) {

	resp = &user_service.GetListInvestorResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			birthday,
			phone_number,
			balance,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "investor"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			first_name   sql.NullString
			last_name    sql.NullString
			birthday     sql.NullString
			phone_number sql.NullString
			balance      sql.NullFloat64
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&first_name,
			&last_name,
			&birthday,
			&phone_number,
			&balance,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, &user_service.Investor{
			Id:          id.String,
			FirstName:   first_name.String,
			LastName:    last_name.String,
			DateOfBirth: birthday.String,
			PhoneNumber: phone_number.String,
			Balance:     float32(balance.Float64),
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *investorRepo) Update(ctx context.Context, req *user_service.UpdateInvestor) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "investor"
			SET
				first_name = :first_name,
				last_name = :last_name,
				birthday = :birthday,
				phone_number = :phone_number,
				balance = :balance,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"birthday":     req.GetDateOfBirth(),
		"phone_number": req.GetPhoneNumber(),
		"balance":      req.GetBalance(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *investorRepo) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) (*user_service.InvestorEmpty, error) {

	query := `DELETE FROM "investor" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &user_service.InvestorEmpty{}, nil
}
