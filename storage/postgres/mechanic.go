package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/helper"
)

type mechanicRepo struct {
	db *pgxpool.Pool
}

func NewMechanicRepo(db *pgxpool.Pool) *mechanicRepo {
	return &mechanicRepo{
		db: db,
	}
}

func (r *mechanicRepo) Create(ctx context.Context, req *user_service.CreateMechanic) (string, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "mechanik"(id, name,birthday ,phone_number, created_at)
		VALUES ($1, $2, $3,$4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		&id,
		&req.Name,
		&req.Birthday,
		&req.PhoneNumber,
	)

	if err != nil {
		return "", err
	}

	return id, nil
}

func (r *mechanicRepo) GetByID(ctx context.Context, req *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error) {
	var (
		query string

		id          sql.NullString
		name        sql.NullString
		birthday    sql.NullString
		phoneNumber sql.NullString
		created_at  sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			birthday,
			phone_number,
			created_at
		FROM "mechanik"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&birthday,
		&phoneNumber,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &user_service.Mechanic{
		Id:          id.String,
		Name:        name.String,
		Birthday:    birthday.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   created_at.String,
	}, nil
}

func (r *mechanicRepo) GetList(ctx context.Context, req *user_service.GetListMechanicRequest) (*user_service.GetListMechanicResponse, error) {

	var (
		resp   = &user_service.GetListMechanicResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			birthday,
			phone_number
		FROM "mechanik"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			birthday    sql.NullString
			phoneNumber sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&birthday,
			&phoneNumber,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Mechanics = append(resp.Mechanics, &user_service.Mechanic{
			Id:          id.String,
			Name:        name.String,
			Birthday:    birthday.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return resp, nil
}

func (r *mechanicRepo) Update(ctx context.Context, req *user_service.UpdateMechanic) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"mechanik"
		SET
			name = :name,
			birthday = :birthday,
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"birthday":     req.Birthday,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *mechanicRepo) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM mechanik WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
