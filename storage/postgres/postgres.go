package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_user_service/config"
	"gitlab.com/car_rent1/cr_go_user_service/storage"
)

type Store struct {
	db       *pgxpool.Pool
	mechanic *mechanicRepo
	branch   *branchRepo
	investor storage.InvestorRepoI
	client   storage.ClientRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Mechanic() storage.MechanicRepoI {

	if s.mechanic == nil {
		s.mechanic = NewMechanicRepo(s.db)
	}

	return s.mechanic

}

func (s *Store) Branch() storage.BranchRepoI {

	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch

}
func (s *Store) Investor() storage.InvestorRepoI {
	if s.investor == nil {
		s.investor = NewInvestorRepo(s.db)
	}
	return s.investor
}

func (s *Store) Client() storage.ClientRepoI {
	if s.client == nil {
		s.client = NewClientRepo(s.db)
	}
	return s.client
}
