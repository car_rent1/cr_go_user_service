package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_user_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_user_service/pkg/helper"
)

type clientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) *clientRepo {
	return &clientRepo{
		db: db,
	}
}

func (c *clientRepo) Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.ClientPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "client" (
				id,
				name,
				surname,
				middle_name,
				phone_number,
				address,
				PNFL_number,
				balance,
				password,
				email,
				login
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8,$9,$10,$11)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Surname,
		req.MiddleName,
		req.PhoneNumber,
		req.Address,
		req.PNFLNumber,
		req.Balance,
		req.Password,
		req.Email,
		req.Login,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.ClientPrimaryKey{Id: id.String()}, nil

}

func (c *clientRepo) GetByPKey(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.Client, err error) {
	query := `
		SELECT
			id,
			name,
			surname,
			middle_name,
			phone_number,
			address,
			PNFL_number,
			balance,
			password,
			email,
			login,
			created_at,
			updated_at
		FROM "client"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		name         sql.NullString
		surname      sql.NullString
		middleName   sql.NullString
		phone_number sql.NullString
		address      sql.NullString
		PNFL_number  sql.NullString
		balance      int
		password     sql.NullString
		email        sql.NullString
		login        sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&surname,
		&middleName,
		&phone_number,
		&address,
		&PNFL_number,
		&balance,
		&password,
		&email,
		&login,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Client{
		Id:          id.String,
		Name:        name.String,
		Surname:     surname.String,
		MiddleName:  middleName.String,
		PhoneNumber: phone_number.String,
		Address:     address.String,
		PNFLNumber:  PNFL_number.String,
		Balance:     int64(balance),
		Password:    password.String,
		Email:       email.String,
		Login:       login.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *clientRepo) GetAll(ctx context.Context, req *user_service.GetListClientRequest) (resp *user_service.GetListClientResponse, err error) {

	resp = &user_service.GetListClientResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			surname,
			middle_name,
			phone_number,
			address,
			PNFL_number,
			balance,
			password,
			email,
			login,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "client"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			surname      sql.NullString
			middleName   sql.NullString
			phone_number sql.NullString
			address      sql.NullString
			PNFL_number  sql.NullString
			balance      int
			password     sql.NullString
			email        sql.NullString
			login        sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&surname,
			&middleName,
			&phone_number,
			&address,
			&PNFL_number,
			&balance,
			&password,
			&email,
			&login,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Clients = append(resp.Clients, &user_service.Client{
			Id:          id.String,
			Name:        name.String,
			Surname:     surname.String,
			MiddleName:  middleName.String,
			PhoneNumber: phone_number.String,
			Address:     address.String,
			PNFLNumber:  PNFL_number.String,
			Balance:     int64(balance),
			Password:    password.String,
			Email:       email.String,
			Login:       login.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *clientRepo) Update(ctx context.Context, req *user_service.UpdateClient) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "client"
			SET
				name = :name,
				surname = :surname,
				middle_name = :middle_name,
				phone_number = :phone_number,
				address = :address,
				PNFL_number = :PNFL_number,
				balance = :balance,
				password = :password,
				email = :email,
				login = :login,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"surname":      req.GetSurname(),
		"middle_name":  req.GetMiddleName(),
		"phone_number": req.GetPhoneNumber(),
		"address":      req.GetAddress(),
		"PNFL_number":  req.GetPNFLNumber(),
		"balance":      req.GetBalance(),
		"password":     req.GetPassword(),
		"email":        req.GetEmail(),
		"login":        req.GetLogin(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *clientRepo) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) (*user_service.ClientEmpty, error) {

	query := `DELETE FROM "client" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &user_service.ClientEmpty{}, nil
}
