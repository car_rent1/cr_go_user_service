CREATE TABLE "investor" (
  "id" UUID PRIMARY KEY NOT NULL,
  "first_name" varchar,
  "last_name" varchar,
  "birthday" date,
  "phone_number" varchar,
  "balance" numeric,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "client" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "surname" varchar,
  "middle_name" varchar,
  "phone_number" varchar NOT NULL,
  "address" varchar,
  "JSHIR_number" varchar NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "mechanik" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "birthday" date,
  "phone_number" varchar NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "branch" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "address" varchar NOT NULL,
  "phone_number" varchar,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);
